FROM python:3.7-stretch

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y bash-completion && \
    awk 'f{if(sub(/^#/,"",$0)==0){f=0}};/^# enable bash completion/{f=1};{print;}' /etc/bash.bashrc > /etc/bash.bashrc.new && \
    mv /etc/bash.bashrc.new /etc/bash.bashrc

RUN set -ex && \
    mkdir ~/.gnupg; \
    echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf; \
    for key in \
        05CE15085FC09D18E99EFB22684A14CF2582E0C5 ; \
    do \
        gpg --keyserver hkp://keyserver.ubuntu.com --recv-keys "$key" ; \
    done

ENV KAPACITOR_VERSION 1.6.2
RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" && \
    case "${dpkgArch##*-}" in \
      amd64) ARCH='amd64';; \
      arm64) ARCH='arm64';; \
      *)     echo "Unsupported architecture: ${dpkgArch}"; exit 1;; \
    esac && \
    wget --no-verbose https://dl.influxdata.com/kapacitor/releases/kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb.asc && \
    wget --no-verbose https://dl.influxdata.com/kapacitor/releases/kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb && \
    gpg --batch --verify kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb.asc kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb && \
    dpkg -i kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb && \
    rm -f kapacitor_${KAPACITOR_VERSION}-1_${ARCH}.deb*

RUN git clone --depth 1 --branch v1.6.2 https://github.com/influxdata/kapacitor.git && \
    cd /kapacitor/udf/agent/py/ && \
    pip install --no-cache-dir . && \
    mkdir /home/kapacitor
    
COPY kapacitor.conf /etc/kapacitor/kapacitor.conf
COPY udf/ /home/kapacitor/udf/

EXPOSE 9092

VOLUME /var/lib/kapacitor
RUN chmod -R 755 /var/lib/kapacitor
    
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["tail", "-f", "/var/lib/kapacitor/kapacitord.err"]

