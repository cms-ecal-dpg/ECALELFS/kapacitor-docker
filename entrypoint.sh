#!/bin/bash
set -e

# KAPACITOR_HOSTNAME=${KAPACITOR_HOSTNAME:-$HOSTNAME}
# export KAPACITOR_HOSTNAME

nohup kapacitord -config /etc/kapacitor/kapacitor.conf > /var/lib/kapacitor/kapacitord.err &

sleep 60

set -x
kapacitor -url http://localhost:9092 define taskmonitor-offline -tick /home/kapacitor/udf/taskmonitor/udf_taskmonitor_offline.tick -dbrp ecal_offline_workflows.autogen
kapacitor -url http://localhost:9092 enable taskmonitor-offline
kapacitor -url http://localhost:9092 define taskmonitor-online -tick /home/kapacitor/udf/taskmonitor/udf_taskmonitor_online.tick -dbrp ecal_prompt_v1.autogen
kapacitor -url http://localhost:9092 enable taskmonitor-online
kapacitor -url http://localhost:9092 define taskmonitor-test -tick /home/kapacitor/udf/taskmonitor/udf_taskmonitor_test.tick -dbrp ecal_online_test.autogen
kapacitor -url http://localhost:9092 enable taskmonitor-test
set +x

exec "$@"
