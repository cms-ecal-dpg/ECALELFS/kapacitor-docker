from kapacitor.udf.agent import Agent, Handler
from kapacitor.udf import udf_pb2
import sys
import json
from copy import deepcopy

import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(name)s: %(message)s')
logger = logging.getLogger()

class TaskHandler(Handler):
    """
    Keep a rolling window of historically normal data
    When a new window arrives use a two-sided t-test to determine
    if the new window is statistically significantly different.
    """
    def __init__(self, agent):
        self._agent = agent
        
        self._tasks = {}

        self._idtags = []
        self._nbatches_safe = 10
        self._statuses = [ "idle",
                           "running",
                           "failed",                         
                           "done" ]

    def snapshot(self):
        """
        Snapshot current handled tasks
        """
        logger.info('saving a shapshot')

        # remove finished tasks
        snapshot = deepcopy(self._tasks)
        for key, task in snapshot.items():
            allj = task['idle']+task['running']+task['failed']+task['done']
            if len(task['done']) == len(allj):
                del self._tasks[key]
        
        response = udf_pb2.Response()
        response.snapshot.snapshot = json.dumps(snapshot).encode()

        return response

    def restore(self, restore_req):
        """
        Reload previous state
        """
        logger.info('restoring previous tasks')
        
        success = False
        msg = ''
        try:
            self._tasks = json.loads(restore_req.snapshot.decode())
            success = True
        except Exception as e:
            success = False
            msg = str(e)

        response = udf_pb2.Response()
        response.restore.success = success
        response.restore.error = msg

        self._statuses = [ "idle",
                           "running",
                           "failed",                         
                           "done" ]
        
        return response
        
    def info(self):
        """
        Respond with which type of edges we want/provide and any options we have.
        """
        logger.info('info')
        
        response = udf_pb2.Response()
        # We will consume single points of data aka stream.
        response.info.wants = udf_pb2.BATCH
        # We will produce single points of data aka stream.
        response.info.provides = udf_pb2.STREAM
        # Get the list of tags to build unique task-id from (comma separated list).
        response.info.options['idtags'].valueTypes.append(udf_pb2.STRING)
        # Get the number of batches to wait for before dropping a completed task
        response.info.options['nbatches_safe'].valueTypes.append(udf_pb2.INT)
        
        return response

    def init(self, init_req):
        """
        Given a list of options initialize this instance of the handler
        """
        logger.info('init')

        for opt in init_req.options:
            if opt.name == 'idtags':
                self._idtags = opt.values[0].stringValue.split(',')
            if opt.name == 'nbatches_safe':
                self._nbatches_safe = opt.values[0].intValue
                
        success = True
        logger.info('init: Monitoring assuming these tags: '+'+'.join(self._idtags))

        response = udf_pb2.Response()
        response.init.success = success

        return response

    def begin_batch(self, begin_req):
        """begin_batch: Do something at the beginning of the batch"""
        logger.info('BEGIN BATCH')
        
    def point(self, point):
        """
        This is the main function where new data are handled
        """
        
        logger.info('reading POINT')
        
        # internal task id string (tags in batch mode are scattered around all fields.
        task_id = ''
        tags = {}
        for k in list(set(self._idtags) & set(point.fieldsString.keys())):
            task_id += '+'+point.fieldsString[k]
            tags[k] = point.fieldsString[k]
        for k in list(set(self._idtags) & set(point.fieldsDouble.keys())):
            task_id += '+'+str(point.fieldsDouble[k])
            tags[k] = point.fieldsDouble[k]
        for k in list(set(self._idtags) & set(point.fieldsInt.keys())):
            task_id += '+'+str(point.fieldsInt[k])
            tags[k] = point.fieldsInt[k]
            
        # create a new internal map of the current task if not already present
        if task_id not in self._tasks.keys():
            self._tasks[task_id] = {status : [] for status in self._statuses}
            self._tasks[task_id]['done_batch_cnt'] = 0

        for status in self._statuses:
            # add this job to the new status
            if point.fieldsDouble[status]>0:
                if point.fieldsString['id'] not in self._tasks[task_id][status]:
                    self._tasks[task_id][status].append(point.fieldsString['id'])
            # remove this job from the previous status
            elif point.fieldsString['id'] in self._tasks[task_id][status]:
                self._tasks[task_id][status].remove(point.fieldsString['id'])                
            
        response = udf_pb2.Response()
        response.point.name = "task_summary"
        response.point.time = point.time
        response.point.group = point.group
        response.point.tags.update(tags)
        
        # compute task summary
        total = 0
        for status in self._statuses:
            response.point.fieldsInt[status] = len(self._tasks[task_id][status])
            total += len(self._tasks[task_id][status])
            
        response.point.fieldsInt['total'] = total

        self._agent.write_response(response)

        logger.info('task %s summary inserted' % task_id)

    def end_batch(self, end_req):
        """begin_batch: cleanup cache after N batch in witch the task appears as complete"""
        snapshot = deepcopy(self._tasks)                
        for key, task in snapshot.items():
            allj = task['idle']+task['running']+task['failed']+task['done']
            if len(task['done']) == len(allj):
                task['done_batch_cnt'] += 1
                if task['done_batch_cnt'] > self._nbatches_safe:
                    del self._tasks[key]
        
        logger.info('END BATCH')

if __name__ == '__main__':
    # Create an agent
    agent = Agent()

    # Create a handler and pass it an agent so it can write points
    h = TaskHandler(agent)

    # Set the handler on the agent
    agent.handler = h

    # Anything printed to STDERR from a UDF process gets captured into the Kapacitor logs.
    logger.info('Starting agent for TaskHandler')
    agent.start()
    agent.wait()
    logger.info('Agent finished')

