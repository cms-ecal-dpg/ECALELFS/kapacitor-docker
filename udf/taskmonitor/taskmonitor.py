from kapacitor.udf.agent import Agent, Handler
from kapacitor.udf import udf_pb2
import sys
import json

import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(name)s: %(message)s')
logger = logging.getLogger()

class TaskHandler(Handler):
    """
    Keep a rolling window of historically normal data
    When a new window arrives use a two-sided t-test to determine
    if the new window is statistically significantly different.
    """
    def __init__(self, agent):
        self._agent = agent

        self._tasks = {}

        self._statuses = [ "idle",
                           "running",
                           "failed",                         
                           "done" ]

    def snapshot(self):
        """
        Snapshot current handled tasks
        """
        logger.info('saving a shapshot')

        # remove finished tasks
        for key, task in self._tasks.items():
            allj = task['idle']+task['running']+task['failed']+task['done']
            if len(task['done']) == len(allj):
                del self._tasks[key]
        
        response = udf_pb2.Response()
        response.snapshot.snapshot = json.dumps(self._tasks).encode()

        return response

    def restore(self, restore_req):
        """
        Reload previous state
        """
        logger.info('restoring previous tasks')
        
        success = False
        msg = ''
        try:
            self._tasks = json.loads(restore_req.snapshot.decode())
            success = True
        except Exception as e:
            success = False
            msg = str(e)

        response = udf_pb2.Response()
        response.restore.success = success
        response.restore.error = msg

        self._statuses = [ "idle",
                           "running",
                           "failed",                         
                           "done" ]
        
        return response
        
    def info(self):
        """
        Respond with which type of edges we want/provide and any options we have.
        """
        logger.info('info')
        
        response = udf_pb2.Response()
        # We will consume single points of data aka stream.
        response.info.wants = udf_pb2.STREAM
        # We will produce single points of data aka stream.
        response.info.provides = udf_pb2.STREAM

        return response

    def init(self, init_req):
        """
        Given a list of options initialize this instance of the handler
        """
        logger.info('init')
        
        success = True
        msg = ''

        response = udf_pb2.Response()
        response.init.success = success
        response.init.error = msg[1:]
        
        return response
    
    def point(self, point):
        """
        This is the main function where new data are handled
        """
        
        logger.info('point received')
        
        # internal task id string
        task_id = '+'.join([v if k!='id' else '' for k,v in point.tags.items()])
        
        # create a new internal map of the current task if not already present
        if task_id not in self._tasks.keys():
            self._tasks[task_id] = {status : [] for status in self._statuses}

        for status in self._statuses:
            # add this job to the new status
            if point.fieldsInt[status]>0:
                if point.tags['id'] not in self._tasks[task_id][status]:
                    self._tasks[task_id][status].append(point.tags['id'])
            # remove this job from the previous status
            elif point.tags['id'] in self._tasks[task_id][status]:
                self._tasks[task_id][status].remove(point.tags['id'])                
            
        response = udf_pb2.Response()
        response.point.name = "task_summary"
        response.point.time = point.time
        response.point.group = point.group
        response.point.tags.update(point.tags)
        response.point.tags.pop('id')
        
        # compute task summary
        total = 0
        for status in self._statuses:
            response.point.fieldsInt[status] = len(self._tasks[task_id][status])
            total += len(self._tasks[task_id][status])
            
        response.point.fieldsInt['total'] = total

        self._agent.write_response(response)

        logger.info('task %s summary inserted' % task_id)
                    
if __name__ == '__main__':
    # Create an agent
    agent = Agent()

    # Create a handler and pass it an agent so it can write points
    h = TaskHandler(agent)

    # Set the handler on the agent
    agent.handler = h

    # Anything printed to STDERR from a UDF process gets captured into the Kapacitor logs.
    logger.info('Starting agent for TaskHandler')
    agent.start()
    agent.wait()
    logger.info('Agent finished')

